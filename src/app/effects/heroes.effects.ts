import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {map, switchMap, catchError} from 'rxjs/operators';
import {of} from 'rxjs';

import * as DataActions from '../actions/heroes.actions';
import {HeroService} from '../services/hero.service';
import {Store} from '@ngrx/store';
import {State} from '../reducers';
import {AddNewHeroBegin, DeleteHeroBegin, EditHeroBegin, LoadHeroBegin} from '../actions/heroes.actions';
import {Router} from '@angular/router';

@Injectable()
export class HeroesEffects {
  constructor(
    private actions$: Actions,
    private heroService: HeroService,
    private router: Router,
    private store: Store<State>,
  ) {
  }

  @Effect()
  loadAllHeroes = this.actions$.pipe(
    ofType(DataActions.ActionTypes.LoadHeroesBegin),
    switchMap(() => {
      return this.heroService.fetchHeroes().pipe(
        map(data => new DataActions.LoadHeroesSuccess({data})),
        catchError(error =>
          of(new DataActions.LoadHeroesFailure({error}))
        )
      );
    })
  );

  @Effect()
  loadHero = this.actions$.pipe(
    ofType(DataActions.ActionTypes.LoadHeroBegin),
    switchMap((data: LoadHeroBegin) => {
      return this.heroService.fetchHero(data.payload).pipe(
        map(hero => new DataActions.LoadHeroSuccess({hero})),
        catchError(error =>
          of(new DataActions.LoadHeroFailure({error}))
        )
      );
    })
  );

  @Effect()
  addNewHero = this.actions$.pipe(
    ofType(DataActions.ActionTypes.AddNewHeroBegin),
    switchMap((data: AddNewHeroBegin) => {
      return this.heroService.addNewHero(data.payload).pipe(
        map(() => {
          this.router.navigate(['/']);
          return new DataActions.AddNewHeroSuccess();
        }),
        catchError(error =>
          of(new DataActions.AddNewHeroFailure({error}))
        )
      );
    })
  );

  @Effect()
  deleteHero = this.actions$.pipe(
    ofType(DataActions.ActionTypes.DeleteHeroBegin),
    switchMap((data: DeleteHeroBegin) => {
      return this.heroService.deleteHero(data.payload).pipe(
        map(() => {
          this.router.navigate(['/']);
          return new DataActions.DeleteHeroSuccess();
        }),
        catchError(error =>
          of(new DataActions.DeleteHeroFailure({error}))
        )
      );
    })
  );

  @Effect()
  editHero = this.actions$.pipe(
    ofType(DataActions.ActionTypes.EditHeroBegin),
    switchMap((data: EditHeroBegin) => {
      return this.heroService.editHero(data.payload).pipe(
        map(() => {
          this.router.navigate(['/']);
          return new DataActions.EditHeroSuccess();
        }),
        catchError(error =>
          of(new DataActions.EditHeroFailure({error}))
        )
      );
    })
  );
}
