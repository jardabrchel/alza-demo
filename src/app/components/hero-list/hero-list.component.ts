import {Component, OnInit} from '@angular/core';
import {getAllHeroes, getAllHeroesLoading, State} from '../../reducers';
import {Store} from '@ngrx/store';
import * as HeroesActions from '../../actions/heroes.actions';
import {Hero} from '../../models/hero';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-hero-list',
  templateUrl: './hero-list.component.html',
  styleUrls: ['./hero-list.component.scss']
})
export class HeroListComponent implements OnInit {
  public heroes$: Observable<Hero[]>;
  public loading$: Observable<boolean>;

  constructor(
    private store: Store<State>,
  ) { }

  ngOnInit() {
    this.loadHeroes();

    this.heroes$ = this.store.select(getAllHeroes);
    this.loading$ = this.store.select(getAllHeroesLoading);
  }

  loadHeroes() {
    this.store.dispatch(new HeroesActions.LoadHeroesBegin());
  }

}
