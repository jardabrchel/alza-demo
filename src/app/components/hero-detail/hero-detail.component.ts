import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Hero} from '../../models/hero';
import {Store} from '@ngrx/store';
import {getHeroDetail, getHeroDetailLoading, State} from '../../reducers';
import {Observable} from 'rxjs';
import * as HeroesActions from '../../actions/heroes.actions';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {take, tap} from 'rxjs/operators';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.scss']
})
export class HeroDetailComponent implements OnInit {
  private editHeroForm: FormGroup;
  private heroId: number;
  public hero$: Observable<Hero>;
  public loading$: Observable<boolean>;

  constructor(
    private route: ActivatedRoute,
    private store: Store<State>,
  ) {
  }

  ngOnInit() {
    this.heroId = Number(this.route.snapshot.params['id']);
    this.loadHero();
    this.initEditForm();

    this.hero$ = this.store.select(getHeroDetail)
      .pipe(
        take(1),
        tap((hero: Hero) => this.editHeroForm.get('name').setValue(hero.name))
      );
    this.loading$ = this.store.select(getHeroDetailLoading);
  }

  loadHero() {
    this.store.dispatch(new HeroesActions.LoadHeroBegin(this.heroId));
  }

  initEditForm() {
    this.editHeroForm = new FormGroup({
      name: new FormControl(null, Validators.required)
    });
  }

  deleteHero() {
    this.store.dispatch(new HeroesActions.DeleteHeroBegin(this.heroId));
  }

  onEditHeroSubmit() {
    this.store.dispatch(new HeroesActions.EditHeroBegin({
      id: this.heroId,
      name: this.editHeroForm.get('name').value,
    } as Hero));
  }

}
