import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import * as HeroesActions from '../../actions/heroes.actions';
import {Store} from '@ngrx/store';
import {State} from '../../reducers';

@Component({
  selector: 'app-add-hero',
  templateUrl: './add-hero.component.html',
  styleUrls: ['./add-hero.component.scss']
})
export class AddHeroComponent implements OnInit {
  private newHeroForm: FormGroup;

  constructor(
    private store: Store<State>,
  ) { }

  ngOnInit() {
    this.newHeroForm = new FormGroup({
      name: new FormControl(null, Validators.required)
    });
  }

  onFormSubmit() {
    this.store.dispatch(new HeroesActions.AddNewHeroBegin(this.newHeroForm.get('name').value));
    // Nejaky alert message system by mohl zobrazit success/failure
  }
}


