import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HeroListComponent} from './components/hero-list/hero-list.component';
import {HeroDetailComponent} from './components/hero-detail/hero-detail.component';
import {AddHeroComponent} from './components/add-hero/add-hero.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'hero-list',
    pathMatch: 'full',
  },
  {
    path: 'hero-list',
    component: HeroListComponent
  },
  {
    path: 'hero-detail/:id',
    component: HeroDetailComponent,
  },
  {
    path: 'add-hero',
    component: AddHeroComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
