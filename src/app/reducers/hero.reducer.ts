import * as fromData from '../actions/heroes.actions';
import {Hero} from '../models/hero';

export interface HeroesState {
  allHeroes: Hero[];
  loading: boolean;
  error: any;
}

export const initialState: HeroesState = {
  allHeroes: [],
  loading: false,
  error: null
};

export function reducer(
  state = initialState,
  action: fromData.ActionsUnion
): HeroesState {
  switch (action.type) {
    case fromData.ActionTypes.LoadHeroesBegin: {
      return {
        ...state,
        loading: true,
        error: null
      };
    }

    case fromData.ActionTypes.LoadHeroesSuccess: {
      return {
        ...state,
        loading: false,
        allHeroes: action.payload.data
      };
    }

    case fromData.ActionTypes.LoadHeroesFailure: {
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };
    }

    default: {
      return state;
    }
  }
}

export const getAllHeroes = (state: HeroesState) => state.allHeroes;
export const getAllHeroesLoading = (state: HeroesState) => state.loading;
