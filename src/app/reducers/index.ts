import {ActionReducerMap, createSelector, MetaReducer} from '@ngrx/store';
import {environment} from '../../environments/environment';
import * as fromHeroes from './hero.reducer';
import * as fromHeroDetail from './hero-detail.reducer';

export interface State {
  heroes: fromHeroes.HeroesState;
  heroDetail: fromHeroDetail.HeroState;
}

export const reducers: ActionReducerMap<State> = {
  heroes: fromHeroes.reducer,
  heroDetail: fromHeroDetail.reducer,
};

export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];

/* Selectors */

export const getHeroesState = (state: State) => state.heroes;
export const getAllHeroes = createSelector(
  getHeroesState,
  fromHeroes.getAllHeroes
);
export const getAllHeroesLoading = createSelector(
  getHeroesState,
  fromHeroes.getAllHeroesLoading
);

/* Hero detail state */

export const getHeroDetailState = (state: State) => state.heroDetail;
export const getHeroDetail = createSelector(
  getHeroDetailState,
  fromHeroDetail.getHero
);
export const getHeroDetailLoading = createSelector(
  getHeroDetailState,
  fromHeroDetail.getHeroLoading
);

