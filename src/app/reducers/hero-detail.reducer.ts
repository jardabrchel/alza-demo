import * as fromData from '../actions/heroes.actions';
import {Hero} from '../models/hero';

export interface HeroState {
  hero: Hero;
  loading: boolean;
  error: any;
}

export const initialState: HeroState = {
  hero: null,
  loading: false,
  error: null
};

export function reducer(
  state = initialState,
  action: fromData.ActionsUnion
): HeroState {
  switch (action.type) {
    case fromData.ActionTypes.LoadHeroBegin: {
      return {
        ...state,
        loading: true,
        error: null
      };
    }

    case fromData.ActionTypes.LoadHeroSuccess: {
      return {
        ...state,
        loading: false,
        hero: action.payload.hero
      };
    }

    case fromData.ActionTypes.LoadHeroFailure: {
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };
    }

    default: {
      return state;
    }
  }
}

export const getHero = (state: HeroState) => state.hero;
export const getHeroLoading = (state: HeroState) => state.loading;
