import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({providedIn: 'root'})
export class HttpService {
    private url = 'https://api.shapez.fit/alza-demo/';
    private headers: any;
    private apiKey = '95WdZCRsQKEwQgQylKNBLyjxUGgbw2BNJ3Y3Sg9tMK6S9nBfONnZo0J0MbngKEwQ';

    constructor(public http: HttpClient) {
      this.presetHeaders();
    }

    get<T>(apiPath: string): Observable<T> {
        return this.http.get<T>(this.url + apiPath, {headers: this.headers});
    }

    post<T>(apiPath: string, data: any): Observable<T> {
        return this.http.post<T>(this.url + apiPath, data, {headers: this.headers});
    }

    patch<T>(apiPath: string, data: any): Observable<T> {
        return this.http.patch<T>(this.url + apiPath, data, {headers: this.headers});
    }

    delete<T>(apiPath): Observable<T> {
        return this.http.delete<T>(this.url + apiPath, {headers: this.headers});
    }

    private presetHeaders() {
        this.headers = new HttpHeaders()
            .set('APIKey', this.apiKey);
    }

}
