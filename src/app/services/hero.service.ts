import {Injectable} from '@angular/core';
import {HttpService} from './http.service';
import {Observable} from 'rxjs';
import {Hero} from '../models/hero';

@Injectable({providedIn: 'root'})
export class HeroService {

  constructor(public httpService: HttpService,
  ) {
  }

  fetchHeroes(): Observable<Hero[]> {
    const apiPath = 'heroes';
    return this.httpService.get<Hero[]>(apiPath);
  }

  fetchHero(id: number): Observable<Hero> {
    const apiPath = `hero/${id}`;
    return this.httpService.get<Hero>(apiPath);
  }

  addNewHero(name: string): Observable<void> {
    const apiPath = 'add-hero';
    const apiData = {name};
    return this.httpService.post<void>(apiPath, apiData);
  }

  deleteHero(id: number): Observable<void> {
    const apiPath = `hero/${id}`;
    return this.httpService.delete<void>(apiPath);
  }

  editHero(hero: Hero): Observable<Hero> {
    const apiPath = `hero/${hero.id}`;
    const apiData = {name: hero.name};
    return this.httpService.patch<Hero>(apiPath, apiData);
  }

}
