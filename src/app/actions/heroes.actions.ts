import {Action} from '@ngrx/store';
import {Hero} from '../models/hero';

export enum ActionTypes {
  LoadHeroesBegin = '[ALL HEROES] Load heroes begin',
  LoadHeroesSuccess = '[ALL HEROES] Load heroes success',
  LoadHeroesFailure = '[ALL HEROES] Load heroes failure',
  AddNewHeroBegin = '[NEW HERO] Add new hero begin',
  AddNewHeroSuccess = '[NEW HERO] Add new hero success',
  AddNewHeroFailure = '[NEW HERO] Add new hero failure',
  DeleteHeroBegin = '[DELETE HERO] Delete hero begin',
  DeleteHeroSuccess = '[DELETE HERO] Delete hero success',
  DeleteHeroFailure = '[DELETE HERO] Delete hero failure',
  LoadHeroBegin = '[LOAD HERO] Load hero begin',
  LoadHeroSuccess = '[LOAD HERO] Load hero success',
  LoadHeroFailure = '[LOAD HERO] Load hero failure',
  EditHeroBegin = '[EDIT HERO] Edit hero begin',
  EditHeroSuccess = '[EDIT HERO] Edit hero success',
  EditHeroFailure = '[EDIT HERO] Edit hero failure',
}

/* Load Heroes */
export class LoadHeroesBegin implements Action {
  readonly type = ActionTypes.LoadHeroesBegin;
}

export class LoadHeroesSuccess implements Action {
  readonly type = ActionTypes.LoadHeroesSuccess;

  constructor(public payload: { data: Hero[] }) {
  }
}

export class LoadHeroesFailure implements Action {
  readonly type = ActionTypes.LoadHeroesFailure;

  constructor(public payload: { error: any }) {
  }
}

/* Load Hero detail */
export class LoadHeroBegin implements Action {
  readonly type = ActionTypes.LoadHeroBegin;

  constructor(public payload: number) {
  }
}

export class LoadHeroSuccess implements Action {
  readonly type = ActionTypes.LoadHeroSuccess;

  constructor(public payload: { hero: Hero }) {
  }
}

export class LoadHeroFailure implements Action {
  readonly type = ActionTypes.LoadHeroFailure;

  constructor(public payload: { error: any }) {
  }
}

/* Add new hero */
export class AddNewHeroBegin implements Action {
  readonly type = ActionTypes.AddNewHeroBegin;

  constructor(public payload: string) {
  }
}

export class AddNewHeroSuccess implements Action {
  readonly type = ActionTypes.AddNewHeroSuccess;
}

export class AddNewHeroFailure implements Action {
  readonly type = ActionTypes.AddNewHeroFailure;

  constructor(public payload: { error: any }) {
  }
}

/* Delete hero */
export class DeleteHeroBegin implements Action {
  readonly type = ActionTypes.DeleteHeroBegin;

  constructor(public payload: number) {
  }
}

export class DeleteHeroSuccess implements Action {
  readonly type = ActionTypes.DeleteHeroSuccess;
}

export class DeleteHeroFailure implements Action {
  readonly type = ActionTypes.DeleteHeroFailure;

  constructor(public payload: { error: any }) {
  }
}

/* Edit hero */
export class EditHeroBegin implements Action {
  readonly type = ActionTypes.EditHeroBegin;

  constructor(public payload: Hero) {
  }
}

export class EditHeroSuccess implements Action {
  readonly type = ActionTypes.EditHeroSuccess;
}

export class EditHeroFailure implements Action {
  readonly type = ActionTypes.EditHeroFailure;

  constructor(public payload: { error: any }) {
  }
}

export type ActionsUnion =
  LoadHeroesBegin
  | LoadHeroesSuccess
  | LoadHeroesFailure
  | LoadHeroBegin
  | LoadHeroSuccess
  | LoadHeroFailure
  | AddNewHeroBegin
  | AddNewHeroSuccess
  | AddNewHeroFailure;
